
import { uploadData, asmcrypto_encryptUploadBuffer, asmcrypto_downloadDecryptBuffer, getSSEkeys } from 'asclepios-sse-client';
import { v4 as uuidv4 } from 'uuid';

export default class AsclepiosResource {

   constructor(type, path, meta, file, keyid) {
      this.type = type;
      this.path = path;
      this.meta = meta;
      this.file = file;
      this.keyid = keyid;
   }

   async read(keycloak) {
      // get SSE key from Keytray
      let { encKey } = await getSSEkeys(
         this.keyid,
         keycloak.tokenParsed.preferred_username,
         keycloak.token
      )

      // download and decrypt data
      return await asmcrypto_downloadDecryptBuffer(this.path, encKey, keycloak.token);
   }

   create(keycloak, onProgress) {
      return new Promise((resolve,reject) => {
         onProgress(0);

         var reader = new FileReader();

         reader.onload = async (event) => {
            // get SSE keys from Keytray
            let { verKey, encKey } = await getSSEkeys(
               this.keyid,
               keycloak.tokenParsed.preferred_username,
               keycloak.token
            )

            onProgress(0.25);

            await asmcrypto_encryptUploadBuffer(event.target.result, this.path, encKey, keycloak.token);
            onProgress(0.75);

            // encrypt and upload metadata
            await uploadData(
               { type: this.type, asclepiosPath: this.path, originalFileName: this.file.name, ...this.meta },
               uuidv4(),
               verKey,
               encKey,
               this.keyid,
               true,
               keycloak.token
            )

            onProgress(1);
            resolve();
         };

         reader.readAsArrayBuffer(this.file);
      }
   )}
}
