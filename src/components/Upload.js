import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import Countdown from 'components/Countdown';
import Experiment from 'xnat/Experiment';
import Project from 'xnat/Project';
import Subject from 'xnat/Subject';
import AsclepiosResource from '../asclepios/Resource';

import { uploadStates as STATES, KeycloakContext } from '../constants'

export default class Upload extends Component {

  static contextType = KeycloakContext;

  static propTypes = {
    bundle: PropTypes.object.isRequired,
    onFinish: PropTypes.func,
    onUpdateStatus: PropTypes.func,
    project: PropTypes.instanceOf(Project),
    subject: PropTypes.instanceOf(Subject),
    experiment: PropTypes.instanceOf(Experiment),
    asclepiosPath: PropTypes.string,
  }

  static defaultProps = {
    onFinish: {},
    onUpdateStatus: {},
    asclepiosPath: null,
  }

  state = {
    error: null,
    progress: 0,
    uploadStatus: STATES.DESTINED,
    experiment: {},
    fileName: null,
  }

  updateStatus = (uploadStatus) => {
    this.setState({ uploadStatus });
    this.props.onUpdateStatus(this.props.bundle, uploadStatus);
  }

  finish = (error) => {
    this.setState({
      error,
      progress: 0,
    });

    this.updateStatus(error ? STATES.FAILED : STATES.READY)

  }

  getExperiment = () => {
    let { subject, experiment } = this.props;

    if (experiment) {
      this.setState({experiment_label: experiment.data.experiment_label})
      return experiment;
    }

    let experiment_label = uuidv4();

    const options = {
      label: experiment_label,
      type: 'snet02:sleepResearchSessionData',
    };

    experiment = subject.createExperiment(options);

    this.setState({experiment_label})

    return experiment;
  }

  uploadEdf = async (projectId, subjectId, experiment, keyid) => {
    let edf = this.props.bundle.edf;
    let file = edf.file.file;
    let fileName = uuidv4();
    let experimentId = experiment.data.experiment;

    this.setState({fileName})

    this.updateStatus(STATES.UPLOADING);

    // create scan
    await experiment.createScan({
      scan: fileName,
      type: 'snet02:encPsgScanData',
      keyid: keyid,
    });

    let path = `${keyid}/${projectId}/${subjectId}/${experimentId}/${fileName}.edf`;
    let meta = await edf.readHeaderFlat();

    meta.project = projectId;
    meta.subject = subjectId;
    meta.experiment = experimentId;

    let asclepiosResource = new AsclepiosResource('snet02:encPsgScanData', path, meta, file, keyid);

    await asclepiosResource.create(this.context, progress => {
      progress = progress * 100;
      this.setState({ progress });
      if (progress === 100) this.updateStatus(STATES.DONE);
    });

    if (this.props.bundle.artifacts) {
      this.uploadAnnotation(projectId, subjectId, experiment, keyid, fileName + '-annotation');
    }
  }

  uploadAnnotation = async (projectId, subjectId, experiment, keyid, fileName) => {
    let file = this.props.bundle.artifacts.file.file;
    let experimentId = experiment.data.experiment;

    this.setState({fileName})

    this.updateStatus(STATES.UPLOADING);

    await experiment.createScan({
      scan: fileName,
      type: 'snet02:encPsgAnnotationData',
      keyid: keyid,
    })

    let path = `${keyid}/${projectId}/${subjectId}/${experimentId}/${fileName}.csv`;

    let meta = {}
    meta.project = projectId;
    meta.subject = subjectId;
    meta.experiment = experimentId;

    let asclepiosResource = new AsclepiosResource('snet02:encPsgAnnotationData', path, meta, file, keyid);

    await asclepiosResource.create(this.context, progress => {
      progress = progress * 100;
      this.setState({ progress });
      if (progress === 100) this.updateStatus(STATES.DONE);
    });
  }

  startUpload = async () => {
    let error = null;

    try {
      let projectId = this.props.project.data.project;
      let subjectId = this.props.subject.data.subject;
      let experiment = await this.getExperiment();
      let keyid = await this.props.subject.getAsclepiosKeyId();

      if(!this.props.asclepiosPath) {
        await this.uploadEdf(projectId, subjectId, experiment, keyid)
      } else {
        let fileName = this.props.bundle.artifacts.file.name.replace(/\.csv$/, '');
        await this.uploadAnnotation(projectId, subjectId, experiment, keyid, fileName)
      }
    }
    catch (e) {
      console.error(e);
      error = e.statusText;
      this.finish(error);
    }
  }

  render() {
    const { uploadStatus, error, experiment_label, fileName } = this.state;
    const { project, subject } = this.props;
    const progress = `${this.state.progress.toFixed(2)}%`;
    const Alert = ({ type = 'info', children }) => <div className={`alert alert-${type}`}>{children}</div>;

    const projectId = project.data.project;
    const subjectId = subject.data.subject;

    switch (uploadStatus) {

      case STATES.UPLOADING:
        return (
          <div className="list-group m-b-1">
            <Alert>
              <span>Uploading</span>
              <div className="progress-bar progress-info">
                <div style={{ width: progress }}>{progress}</div>
              </div>
            </Alert>
          </div>
        );

      case STATES.UPLOADED:
        return <Alert><span className="loading">Starting Analysis</span></Alert>;

      case STATES.POLLING:
        return <Alert>Waiting <Countdown seconds={600} onTargetReached={() => this.finish('Pipeline failed')} /> for Results.</Alert>;

      case STATES.PREPARING:
        return <Alert><span className="loading">Preparing Results</span></Alert>;

      case STATES.DONE:
        return (
          <div className="list-group m-b-1">
            <Alert type="info"><strong>Uploaded</strong> {fileName}.</Alert>
            <button onClick={() => this.finish(false)}>Done</button>
            <a href={`/xnat/data/projects/${projectId}/subjects/${subjectId}/experiments/${experiment_label}`}>View On XNAT</a>
          </div>
        );

      case STATES.FAILED:
        return (
          <div className="list-group m-b-1">
            <Alert type="error"><strong>Error</strong> {error}.</Alert>
            <p>Upload <code>{fileName}</code> to <code>{projectId}/{subjectId}/{experiment_label}</code>.</p>
            <button onClick={this.startUpload}>Retry Upload</button>
          </div>
        );

      default: // STATES.DESTINED and no state (which should not happen)
        return (
          <div className="list-group m-b-1">
            <Alert type="info"><strong>Upload</strong></Alert>
            <p>Upload <code>{fileName}</code> to <code>{projectId}/{subjectId}/{experiment_label}</code>.</p>
            <button onClick={this.startUpload}>Start Upload</button>
          </div>
        );

    }
  }

}
