import * as utils from './utils';
import { getJSON } from './http'
import Base from './Base';
import Experiment from './Experiment';

export default class Subject extends Base {

   static className = "subject";;

   static urls = {
      all: '{host}/projects/{project}/subjects',
      one: '{host}/projects/{project}/subjects/{subject}',
      new: '{host}/projects/{project}/subjects/{subject}?{query}',
   }

   create = utils.create()
   getExperiments = utils.getChildren(Experiment)
   createExperiment = utils.createChild(Experiment)

   constructor(data) {
      super();
      this.initialize(data);
   }

   initialize(data) {
      this.data = utils.rename(data, {
         ID: 'subject',
         label: 'subject_label',
      });
      this.data.query = this.getQuery();
   }

   getQuery() {
      return this.data.type || '';
   }

   async getAsclepiosKeyId() {
      // ugly custom parsing for custom field data
      let url = '{host}/projects/{project}/subjects/{subject}?format=json'
      const response = await getJSON(utils.tpl(url, this.data));
      const field = response.items[0].children
         .find(c => c.field === "fields/field")
      const item = field.items
         .find(i => i.data_fields.name === 'keyid')
      return item.data_fields.field
   }
}
