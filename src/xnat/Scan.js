import * as utils from './utils';
// import * as http from './http';
import Base from './Base';

export default class Scan extends Base {

   static className = "scan";

   static urls = {
      all: '{host}/projects/{project}/subjects/{subject}/experiments/{experiment}/scans/',
      one: '{host}/projects/{project}/subjects/{subject}/experiments/{experiment}/scans/{scan}',
      new: '{host}/projects/{project}/subjects/{subject}/experiments/{experiment}/scans/{scan}?{query}',
   }

   type = 'snet02:encPsgScanData'

   create = utils.create()

   constructor(data) {
      super();
      this.initialize(data);
   }

   initialize(data) {
      this.data = utils.rename(data, {
         ID: 'scan',
      });
      this.data.query = this.getQuery();
   }

   getQuery() {
      const type = this.data.type || this.type;
      const date = utils.getFormattedDate();
      const keyid = this.data.keyid;
      return `${type}/keyid=${keyid}&xsiType=${type}&date=${date}`;
   }

}
