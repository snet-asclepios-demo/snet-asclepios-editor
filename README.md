# snet-asclepios-editor

A performant web-based visualization, assessment and collaboration tool for multidimensional biosignals. Using XNAT and ASCLEPIOS for storage.

<p align="center">
  <img src="screenshot.png">
</p>


## Build

* Create the file `.env` and add something like the following:

```env
REACT_APP_IN_BROWSER=1
REACT_APP_XNAT_API_URL=http://localhost/xnat/REST

TA_URL=http://localhost/ta
SSE_URL=http://localhost/sse
SALT=ZWdhYndlZmc=
IV=bGd3YmFnd2c=
ITER=10000
KS=256
TS=64
HASH_LEN=256
CHUNK_SIZE=32768
NO_CHUNKS_PER_UPLOAD=30
SALT_TA=ZRVja4LFrFY=
IV_TA=YXp5bWJscWU=
ITER_TA=10000
KS_TA=128
TS_TA=64
SGX_ENABLE=false
CP_ABE_URL=http://localhost:8084
AUTH=true
DEBUG=true
SMALL_FILE=0

```

* Then run:

```sh
npm install
npm run build
```


## Develop

* asclepios-sse-client ships unconfigured and does not have a run time config
  interface so it needs to be manually configured like this:

```sh
. env

find build/static/js/ -type f -exec sed -i "s|ta_url|$TA_URL|g" {} \;
find build/static/js/ -type f -exec sed -i "s|sse_url|$SSE_URL|g" {} \;
find build/static/js/ -type f -exec sed -i "s|salt_value|$SALT|g" {} \;
find build/static/js/ -type f -exec sed -i "s|iv_value|$IV|g" {} \;
find build/static/js/ -type f -exec sed -i "s|iter_value|$ITER|g" {} \;
find build/static/js/ -type f -exec sed -i "s|ks_value|$KS|g" {} \;
find build/static/js/ -type f -exec sed -i "s|ts_value|$TS|g" {} \;
find build/static/js/ -type f -exec sed -i "s|mode_value|$MODE|g" {} \;
find build/static/js/ -type f -exec sed -i "s|adata_value|$ADATA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|adata_len_value|$ADATA_LEN|g" {} \;
find build/static/js/ -type f -exec sed -i "s|hash_length_value|$HASH_LEN|g" {} \;
find build/static/js/ -type f -exec sed -i "s|chunk_size_value|$CHUNK_SIZE|g" {} \;
find build/static/js/ -type f -exec sed -i "s|no_chunks_per_upload_value|$NO_CHUNKS_PER_UPLOAD|g" {} \;
find build/static/js/ -type f -exec sed -i "s|salt_ta_value|$SALT_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|iv_ta_value|$IV_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|iter_ta_value|$ITER_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|ks_ta_value|$KS_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|ts_ta_value|$TS_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|mode_ta_value|$MODE_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|adata_ta_value|$ADATA_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|adata_len_ta_value|$ADATA_LEN_TA|g" {} \;
find build/static/js/ -type f -exec sed -i "s|sgx_enable_value|$SGX_ENABLE|g" {} \;
find build/static/js/ -type f -exec sed -i "s|cp_abe_url|$CP_ABE_URL|g" {} \;
find build/static/js/ -type f -exec sed -i "s|debug_value|$DEBUG|g" {} \;
find build/static/js/ -type f -exec sed -i "s|auth_value|$AUTH|g" {} \;
find build/static/js/ -type f -exec sed -i "s|small_file_value|$SMALL_FILE|g" {} \;
```

* start a dev server

```
npm start
```

## Other EDF-Viewer

- [SigViewer](https://github.com/cbrnr/sigviewer) (C++)
- [NSRR Cross Cohort EDF Viewer](https://sleepdata.org/community/tools/nsrr-edf-viewer) (Matlab)
- [EDFbrowser](https://github.com/Teuniz/EDFbrowser) (C++)
   - [The EDF format](https://www.teuniz.net/edfbrowser/edf%20format%20description.html)
   - [Some EDF/BDF testfiles](https://www.teuniz.net/edf_bdf_testfiles/)


## Related formats

- [Extensible Data Format (XDF)](https://github.com/sccn/xdf) ([Specifications](https://github.com/sccn/xdf/wiki/Specifications))
- [OpenXDF](http://www.openxdf.org/)


## More Links

- [Canvas vs. SVG](http://www.hammerlab.org/2015/10/13/svg-canvas-the-pileup-js-journey/)
- [Dygrahps](https://github.com/danvk/dygraphs)
- [Dygraphs Dynamic Zooming Example](https://github.com/kaliatech/dygraphs-dynamiczooming-example)

